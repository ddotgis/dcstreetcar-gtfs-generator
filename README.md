# DC Streetcar Conversion to GTFS#

### This repository contains scripts which take DDOT's GIS Streetcar line and stop geometries and convert into GTFS data ###

* Using DDOT's Streetcar stops and routes GIS data, this script creates a linearly referenced network, and joins the stops to that network.  It makes use of these data to generate a fully functional GTFS feed file which is used by Google Maps and other systems.  DDOT is in the final process of having this GTFS file reviewed by Google.  Once approved, it will be one of the modes of travel as part of a standard Google Maps directions request.
* Version:  1.0

### How do I get set up? ###

* Summary of set up:
** In order to make use of these scripts, you must download the DDOT GIS Streetcar data (stops and routes) and reference these data locally in the script.  Data will be available via this map service:
http://maps2.dcgis.dc.gov/dcgis/rest/services/DDOT/Streetcar/MapServer
* Configuration:  
**You will need to alter the 'Database Connection' location to point to these data.
* Dependencies
**ArcGIS 10 or higher
**Python 2.7

### Contribution guidelines ###

* Currently there are no formal guidelines for how to contribute changes or improvements.  I do ask that you submit an issue on this repo and I will respond as soon as possible.  

### Who do I talk to? ###

* james.graham2@dc.gov
* http://ddot.in.dc.gov
